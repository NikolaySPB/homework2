﻿using System;

namespace Lesson2
{
    class Product
    {
        public string Name;         // Наименование
        public double Weight;        // Вес
        public double Protein;       // Белки
        public double Fat;           // Жиры
        public double Carbohydrates; // Углеводы
        public double Energy;        // Эн.ценность

        // Конструктор
        public Product(string name, double weight, double protein, double fat, double carbohydrates, double energy)
        {
            Name = name;
            Weight = weight;
            Protein = protein;
            Fat = fat;
            Carbohydrates = carbohydrates;
            Energy = energy;
        }

        // Метод расчета всех параметров на заданное количество килограмм продукта
        public string CalculationInKilograms(double kilogram)
        {
            double numberOfProducts = kilogram * 1000 / Weight;
            double protein = numberOfProducts * Protein;
            double fat = numberOfProducts * Fat;
            double carbohydrates = numberOfProducts * Carbohydrates;
            double energy = numberOfProducts * Energy;

            return $"\nПродукт взятый в расчет: {Name}." +
                $"\nНа {kilogram} кг. продукта следующие данные:" +
                $"\n\tединиц продукта {Math.Round(numberOfProducts, 2)} шт.;" +
                $"\n\tбелков:         {Math.Round(protein, 2)};" +
                $"\n\tжиров:          {Math.Round(fat, 2)};" +
                $"\n\tуглеводов:      {Math.Round(carbohydrates, 2)};" +
                $"\n\tэн.ценности:    {Math.Round(energy, 2)}";
        }

        // Перегруженный оператор сложения. Данный оператор создает новый среднеарифметический продукт на основе двух выбранных.
        public static Product operator +(Product product1, Product product2)
        {
            return new Product($"Из продукта {product1.Name} и {product2.Name}, получен новый продукт",
                (product1.Weight + product2.Weight) / 2,
                (product1.Protein + product2.Protein) / 2,
                (product1.Fat + product2.Fat) / 2,
                (product1.Carbohydrates + product2.Carbohydrates) / 2,
                (product1.Energy + product2.Energy) / 2);
        }

        // Перегруженный оператор умножения. Данный оператор умножает на заданное количество единиц продукта
        public static Product operator *(Product product, int numberOfProducts)
        {
            return new Product($"Сумма {numberOfProducts} шт. продукта {product.Name} ровна", product.Weight * numberOfProducts, product.Protein * numberOfProducts, product.Fat * numberOfProducts,
                product.Carbohydrates * numberOfProducts, product.Energy * numberOfProducts);
        }

        //Индексатор
        public string this[int index]
        {
            get
            {
                switch (index)
                {
                    case 0: return Name;
                    case 1: return Weight.ToString();
                    case 2: return Protein.ToString();
                    case 3: return Fat.ToString();
                    case 4: return Carbohydrates.ToString();
                    case 5: return Energy.ToString();
                    default: throw new IndexOutOfRangeException();
                }
            }
        }

        // Переопределение ToString
        public override string ToString()
        {
            return $"{Name}: вес {Weight} г., белки {Math.Round(Protein, 2)}, жиры {Math.Round(Fat, 2)}, углеводы {Math.Round(Carbohydrates, 2)}, эн.ценность {Math.Round(Energy, 2)}.";
        }
    }
}
