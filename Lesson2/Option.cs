﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Lesson2
{
    static class Option
    {
        // Метод расширения для класса. Метод сравнивает на идентичность продуктов по всем характеристикам (без наименования продукта).
        public static bool IsEquality(this Product product1, Product product2)
        {
            return (product1.Weight == product2.Weight
                & product1.Protein == product2.Protein
                & product1.Fat == product2.Fat
                & product1.Carbohydrates == product2.Carbohydrates
                & product1.Energy == product2.Energy);
        }
    }
}
