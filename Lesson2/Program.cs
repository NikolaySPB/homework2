﻿using System;

namespace Lesson2
{
    class Program
    {
        static void Main(string[] args)
        {
            /* Можно все вывести на консоль, где будет удобно сверять данные.
             * Использованные операторы в коде: x.y / a[i] / f(x) / new / x++ / true / false / x * y /  x / y  / x = y / x + y / x == y / x & y / c ? t : f  
             */
            // Создадим четыре продукта (с использованием конструктора): яблоко, грушу, апельсин и банан
            Product apple = new Product("Яблоко", 176, 0.88, 0, 20.1, 85);
            Product pear = new Product("Груша", 180, 0.9, 0, 19.1, 74);
            Product orange = new Product("Апельсин", 150, 1.2, 0, 12.9, 57);
            Product banana = new Product("Банан", 120, 2.1, 0, 26.5, 105);

            // Выведем на консоль все продукты
            Console.WriteLine("Все продукты\n");

            Console.WriteLine($"{apple}\n{pear}\n{orange}\n{banana}");

            // Проверяем метод расчета на заданное количество кг. и сразу выводим на консоль.
            Console.WriteLine("\n\nПроверяем метод расчета на заданное количество кг.");

            Console.WriteLine(apple.CalculationInKilograms(4));
            Console.WriteLine(pear.CalculationInKilograms(10));
            Console.WriteLine(orange.CalculationInKilograms(15));
            Console.WriteLine(banana.CalculationInKilograms(18));


            // Оператор равенства и условный оператор
            Console.WriteLine("\n\nОператор равенства и условный оператор\n");

            bool equality = apple.Weight == pear.Weight ? true : false;

            Console.WriteLine(equality);

            // Создадим новый продукт с помощью перегруженного оператора +
            Console.WriteLine("\n\nСоздадим новый продукт с помощью перегруженного оператора +\n");

            Product newProduct1 = apple + pear;
            Product newProduct2 = orange + banana;

            Console.WriteLine($"{newProduct1}\n{newProduct2}");

            // Проверяем перегруженный оператор *
            Console.WriteLine("\n\nПроверяем перегруженный оператор *\n");

            Product sumProduct1 = apple * 3;
            Product sumProduct2 = pear * 5;
            Product sumProduct3 = orange * 7;
            Product sumProduct4 = banana * 9;

            Console.WriteLine($"{sumProduct1}\n{sumProduct2}\n{sumProduct3}\n{sumProduct4}");


            // Проверяем индексатор
            Console.WriteLine("\n\nПроверяем индексатор\n");

            Console.WriteLine($"Это выведем для удобства проверки на консоли\n{apple}\n");

            for (int i = 0; i < 6; i++)
            {
                Console.WriteLine($"Проверяем: apple[{i}] итог: {apple[i]}");
            }


            // Проверяем метод расширения для класса.
            Console.WriteLine("\n\nПроверяем метод расширения для класса\n");

            Console.WriteLine($"Сравниваем характеристики продукта {apple.Name} и {pear.Name}. Результат: {apple.IsEquality(pear)}");
            Console.WriteLine($"Сравниваем характеристики продукта {pear.Name} и {orange.Name}. Результат: {pear.IsEquality(orange)}");
            Console.WriteLine($"Сравниваем характеристики продукта {orange.Name} и {banana.Name}. Результат: {orange.IsEquality(banana)}");
            Console.WriteLine($"Сравниваем характеристики продукта {banana.Name} и {apple.Name}. Результат: {banana.IsEquality(apple)}");

            Product apple2 = new Product("Зеленое яблоко", 176, 0.88, 0, 20.1, 85); // Создадим яблоко с идентичными характеристиками для проверки на true метода расширения для класса

            Console.WriteLine($"Сравниваем характеристики продукта {apple.Name} и {apple2.Name}. Результат: {apple.IsEquality(apple2)}");

            Console.ReadLine();
        }
    }
}
